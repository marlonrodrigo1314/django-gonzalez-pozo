from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PagoForm, DeudaForm
from .models import Pago, Deuda

# Create your views here.

def agregarPago(request):
	if request.method == 'POST':
		pago_form = PagoForm(request.POST)
		if pago_form.is_valid():
			pago_form.save()
		return redirect('mis_pagos')
	else:
		pago_form = PagoForm()
	return render(request,'agregar_pago.html', {'pago_form':pago_form})

def misPagos(request):
	pago = Pago.objects.all()
	contexto = {'pagos': pago}
	return render(request,'mis_pagos.html', contexto)

def misDeudas(request):
	deuda = Deuda.objects.all()
	contexto = {'deudas': deuda}
	return render(request,'mis_deudas.html', contexto)

def pagarDeuda(request):
	if request.method == 'POST':
		pago_deuda_form = DeudaForm(request.POST)
		if pago_deuda_form.is_valid():
			pago_deuda_form.save()
		return redirect('mis_deudas')
	else:
		pago_deuda_form = DeudaForm()
	return render(request,'pagar_deuda.html', {'pago_deuda_form':pago_deuda_form})


"""
def pagarDeuda(request, id):
	contexto = {}
	if request.method == "GET":
		try:
			deuda = Deuda.objects.get(id=id)
		except:
			raise Http404
		contexto['deuda'] = deuda
		return render(request, 'pagar_deuda.html', contexto)

	elif request.method == "POST":
		v = request.POST
		try:
			deu = Deuda.objects.get(id=v['monto'])
			us = User.objects.get(id=request.user.id)
			sql = Deuda.objects.filter(Q(pagador=us) & Q(monto=deu.deuda))
			
			if sql:
				return render(request, 'deuda/pagar_deuda.html', {'error': True})
			else:
				ret = Deuda.objects.create(opciones=deu, encuestado=us)
				if ret:
					return redirect('mis_deudas')
				else:
					return HttpResponse("Error")
"""
