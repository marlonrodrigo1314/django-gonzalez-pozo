from django.contrib import admin
from .models import Categoria, Pago, Deuda, pagoDeuda

# Register your models here.

class CategoriaAdmin(admin.ModelAdmin):
	pass
admin.site.register(Categoria, CategoriaAdmin)


class PagoAdmin(admin.ModelAdmin):
	list_display = ('categoria','producto','monto','deudor','fecha')
admin.site.register(Pago, PagoAdmin)


class DeudaAdmin(admin.ModelAdmin):
	pass
	list_display = ('categoria','producto','monto','pagador')
admin.site.register(Deuda, DeudaAdmin)

class pagoDeudaAdmin(admin.ModelAdmin):
	pass
admin.site.register(pagoDeuda, pagoDeudaAdmin)