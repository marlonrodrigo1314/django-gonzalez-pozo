from django.urls import path
from base import views

urlpatterns = [
    path('base', views.index),
    path('login', views.login)
]