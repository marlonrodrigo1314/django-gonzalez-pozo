from django.shortcuts import render



def index(request):
	template_name = 'base.html'
	data = {}
	return render(request, template_name, data)




def login(request):
	template_name = 'base_login.html'
	data = {}
	return render(request, template_name, data)
